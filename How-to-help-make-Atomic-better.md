#### Front Matter

The Atomic Game Engine is powerful 2D/3D technology developed by industry veterans and contributors from around the world.  It is being used in production environments and runs on Windows, macOS, Android, iOS, Linux, and WebGL.

Atomic is also a lot of work :)
### How to Help
<img align="right" src="https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/images/repo/atomic/wecandoit.jpg" style="margin-left:20px;"/>
- Let others know about the Atomic Game Engine!
 
- Leverage Atomic technology in your product and/or help sponsor development, email us at: info@atomicgameengine.com  

- Answer questions and provide feedback in the [Atomic Community Support Forum](https://discourse.atomicgameengine.com/c/atomic-community-support) and [Atomic Chat](https://gitter.im/AtomicGameEngine/AtomicGameEngine)

- Help maintain current and write new [Atomic Wiki](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Contributing-to-the-Wiki) documentation articles

- Fork [AtomicGameEngine/AtomicExamples](https://github.com/AtomicGameEngine/AtomicExamples) and submit a pull request with current example improvements or a new example

- Help with issues marked **```unassigned: help wanted```** in the [current build milestone](https://github.com/AtomicGameEngine/AtomicGameEngine/milestones)

- Inform us of other ways to help that should be added here :)  

#### Business Inquiries
If you have a business inquiry, please contact us: info@atomicgameengine.com

Thanks!,  
Josh Engebretson  
*Technical Director* 🐪  
THUNDERBEAST GAMES, LLC  
[[images/repo/atomic/josh.jpg]]
