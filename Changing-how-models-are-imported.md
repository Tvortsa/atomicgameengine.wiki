[Back to Cookbook](Atomic-Cookbook)

Importing models into your scene using the Atomic Editor is as easy as dragging and dropping them on the Models folder in the Atomic Editor UI or simply adding them to the Models folder inside the directory structure of your project.

_Adding models with drag and drop_  
[[images/repo/cookbook/model_import_ui.png]]

_Adding models through the file system_  
[[images/repo/cookbook/model_import_fs.png]]

When the editor detects a new model it processes the file and adds it as an asset. The import process leverages the [assimp](http://www.assimp.org/) open asset import library. During the import process assimp is configured to process the model using a default set of parameters. These parameters can be changed if they do not meet your needs.  

By adding an **import.json** file to the settings directory of your project you can control how assimp processes your 3d model. The following is a json file with the _default_ importer settings.

```json
{
    "desktop": {

        "ModelImporter": {
            "aiFlagsDefault": {
                "convertToLeftHanded": true,
                "joinIdenticalVertices": true,
                "triangulate": true,
                "genSmoothNormals": true,
                "limitBoneWeights": true,
                "improveCacheLocality": true,
                "fixInfacingNormals": false,
                "findInvalidData": true,
                "genUVCoords": true,
                "findInstances": true,
                "optimizeMeshes": true,
                "useVertexColors": false
            }
        },

        "TextureImporter": {
            "compressTextures": false
        }
    }

}
```

For example, by default the importer does not process vertex colors in 3d models, leaving flat shaded models untextured. In order to have the importer set the correct technique in the project's material files the following line:  

```"useVertexColors": false```

would change to

```"useVertexColors": true```

Changing your imported model from this:  

[[images/repo/cookbook/model_before.png]]

to this:  

[[images/repo/cookbook/model_after.png]]

