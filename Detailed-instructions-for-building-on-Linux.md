![](https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/setting-up-atomic/detailed-instructions-for-building-on-linux/header.png)

**Introduction**

The Atomic Game Engine can be built on Linux for development in JavaScript/TypeScript, C++, and optionally C#. For JavaScript and TypeScript, the built-in [Monaco](https://github.com/Microsoft/monaco-editor) editor can be used as well as [external editors](Using-TypeScript-with-External-Editors). C# support requires Mono and MonoDevelop to be installed.

If you are having any issues, please visit Atomic's dedicated [Linux community support forum](https://discourse.atomicgameengine.com/c/atomic-community-support/linux)

**1) Installing Mono & MonoDevelop (Optional for C# Support)**

<img align="left" src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/setting-up-atomic/detailed-instructions-for-building-on-linux/monoDevelopLogo.png" height="50" hspace="10" vspace="3">If you need C# support, Mono and MonoDevelop must be installed before building the engine as this process also generates the managed AtomicNET assemblies and native runtime.  These instructions are for Debian-based distributions.  *You can skip to step 2 if you are only interested in C++/JS/TS development or want to install Mono at a later time.*

Start by adding the Mono Project GPG signing key and the package repository to your system:

    # sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

    # echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list

    # sudo apt-get update

If you're using Ubuntu 13.10/Debian 8.0 or later, you will also need to add the mod_mono and libgdiplus repository to your system:

    # echo "deb http://download.mono-project.com/repo/debian wheezy-apache24-compat main" | sudo tee -a /etc/apt/sources.list.d/mono-xamarin.list

    # echo "deb http://download.mono-project.com/repo/debian wheezy-libjpeg62-compat main" | sudo tee -a /etc/apt/sources.list.d/mono-xamarin.list

For older Ubuntu releases (12.04 or 12.10), to enable installation, you'll have to add the following repository:

    # echo "deb http://download.mono-project.com/repo/debian wheezy-libtiff-compat main" | sudo tee -a /etc/apt/sources.list.d/mono-xamarin.list

Now, run `sudo apt-get update`.
After this, you can install all the packages you need:

    # sudo apt-get install referenceassemblies-pcl mono-devel mono-complete monodevelop

**2) Building the Atomic Editor**

<img align="left" src="https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/images/repo/setting-up-atomic/detailed-instructions-for-building-on-linux/atomicGameEngineLogo.png" height="60" hspace="5" vspace="3">Please follow [ these instructions](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Building-Atomic-from-Source#linux) to build the Atomic Editor and other binaries to the ```/Artifacts/AtomicEditor``` folder.  Once built, the editor can be started from this folder.
<br/><br/>
**3) Getting Started with the Atomic Game Engine**

Looking for help getting started? Please see these [help topics](Getting-Started), visit the [Atomic Community site](https://discourse.atomicgameengine.com), or join us in the [Atomic Gitter Chat](https://gitter.im/AtomicGameEngine/AtomicGameEngine).
