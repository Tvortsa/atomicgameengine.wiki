### Prerequisites

The Atomic Editor is very easy to build and requires minimal setup.  You'll need Visual Studio 2015 from Microsoft and CMake: [http://www.cmake.org/download](http://www.cmake.org/download)

### Step 1: Clone and generate the Visual Studio solution

    git clone --recursive https://github.com/AtomicGameEngine/AtomicGameEngine
    cd AtomicGameEngine
    CMake_VS2015.bat

After a few minutes, this will generate an the Visual Studio solution in a folder called **AtomicGameEngine-VS2015** which will be next to your cloned repo folder:

[[images/build/vs2015/vs2015_solution_folder.png]]

### Step 2: Build the Solution

Open the Atomic.sln solution file, set the AtomicEditor project as the startup project, compile and run

[[images/build/vs2015/vs2015.png]] 
