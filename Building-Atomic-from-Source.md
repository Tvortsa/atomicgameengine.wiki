[[images/repo/setting-up-atomic/building-atomic-the-quick-and-easy-way/header.png]]

### Quick and Easy Way:

- [Building on Windows](#windows)
- [Building on macOS](#macos)
- [Building on Linux](#linux)  
- [Optional Build Flags](#atomic-editor-build-flags-advanced)  <sup>Advanced</sup>

> If you are working on Atomic itself or need to debug an Atomic project with an IDE such as Visual Studio, XCode, QtCreator, please see [this article](How-to-set-up-a-Debug-Development-Build)  <sup>Advanced</sup>

> **Please note:** Building from source code is intended for users who want to use/test new features or customize the C++ source code.  For most users, we recommend the **binary releases** available for [download](http://atomicgameengine.com/#download-the-atomic-editor) on the official Atomic Game Engine website.

---

#### Windows
<img align="left" src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/setting-up-atomic/building-atomic-the-quick-and-easy-way/windowsIcon.png" height="40" hspace="8" vspace="0">
1) Make sure to have Visual Studio 2015 or 2017 installed with a C++ compiler and when using VS2015 have CMake on your PATH.
2) Clone the AtomicGameEngine repository recursively:
```
git clone --recursive https://github.com/AtomicGameEngine/AtomicGameEngine
```
3) Double click **Build_AtomicEditor.bat** in the root folder or use
```
Build_AtomicEditor.bat
```
If you have both VS2015 and VS2017 installed, use the ```--vs2017``` flag to select VS2017.

4) Your build of the Atomic Editor will be built in Artifacts/AtomicEditor.

---

#### macOS
<img align="left" src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/setting-up-atomic/building-atomic-the-quick-and-easy-way/macIcon.png" height="40" hspace="8" vspace="0">
1) Install XCode and CMake, making sure CMake is on your path  
2) Clone the AtomicGameEngine repository recursively:  
```
git clone --recursive https://github.com/AtomicGameEngine/AtomicGameEngine
```
3) Open a shell and change directory (`cd`) to the root repository.<br/>
4) Build the Atomic Game Engine using 
```
./Build_AtomicEditor.sh
```  
4) The latest AtomicEditor.app will be built in Artifacts/AtomicEditor  

---

#### Linux
The following instructions will install the Atomic Game Engine without C# support.
To install with C# support, read [this page](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Detailed-instructions-for-building-on-Linux).

<img align="left" src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/setting-up-atomic/building-atomic-the-quick-and-easy-way/debianIcon.png" height="40" hspace="9" vspace="0">
1)  Install prerequisite packages:  
**Ubuntu 14.04 LTS 64 bit, Debian Jessie 8.* 64 bit:** 
 
```
sudo apt-get install build-essential cmake nodejs libgtk-3-dev libasound2-dev libxrandr-dev libgl1-mesa-dev libglu1-mesa-dev
```  
<img align="left" src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/setting-up-atomic/building-atomic-the-quick-and-easy-way/fedoraIcon.png" height="38" hspace="7" vspace="0">
1)  Install prerequisite packages:  
**Fedora/CentOS 7.2 64 bit:**  

```
sudo yum install gcc gcc-c++ libgcc cmake ccache nodejs gtk3-devel alsa-lib-devel libXrandr-devel mesa-libGL-devel mesa-libGLU-devel  
```
2) Clone the AtomicGameEngine repository recursively: 
```
git clone --recursive https://github.com/AtomicGameEngine/AtomicGameEngine
```
3) Open a shell and change directory (`cd`) to the root repository. From here, run<br/> 
```
sh Build_AtomicEditor.sh
```  
4) The AtomicEditor executable will be built in Artifacts/AtomicEditor  

### Atomic Editor Build Flags (Advanced)

The following build flags are supported by the ```Build_AtomicEditor(.sh/.bat)``` commands:

| Flag               | Description                                             |
| ------------------ | --------------------------------------------------------|
| `--help`           | Displays all these flags                                |
| `--with-android`   | Build with Android platform support                     |
| `--with-ios`       | Build with iOS platform support                         | 
| `--with-web`       | Build with WebGL platform support                         |
| `--opengl`         | Enable OpenGL renderer (if on Windows)                  |
| `--d3d11`          | Enable DirectX 11 renderer (if on Windows)              |
| `--debug`          | Build debug version of the editor and associated platform runtimes |
| `--noclean`        | Do not clean before building, useful during development |
| `--nonet`          | Build without AtomicNET C# scripting support            |
| `--with-docs`      | Build and install API documents into the editor (requires npm on path) |
| `--noexamples`    | Don't include examples with editor                      |
| `--task=name`     | Build the specified task (for development)              |
| `--package`          | Packages the editor to Artifacts/Dist                   |

> The `--nonet` flag will automatically be applied if Visual Studio or mono isn't installed on the system

For example, if you want to build the editor without including examples, run the following from your root local source tree:

**Windows**

    Build_AtomicEditor.bat --noexamples

**macOS/Linux**

    ./Build_AtomicEditor.sh --noexamples

