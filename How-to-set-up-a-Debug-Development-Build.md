![](images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/header.png)

# Introduction

Atomic development builds can be used to work on the Atomic Game Engine itself as well as debug Atomic C++ projects.

If you're primarily interested in trying out or testing the latest features, please see [Building Atomic The Quick and Easy Way](Building-Atomic-from-Source) for a simpler method to compiling the engine.

**Jump to:** 

- [**Visual Studio on Windows**](How-to-set-up-a-Debug-Development-Build#setting-up-a-development-build-with-visual-studio)

- [**QtCreator on Linux and macOS**](How-to-set-up-a-Debug-Development-Build#setting-up-a-development-build-with-qtcreator)

- **XCode** - It is possible to use XCode, though a guide needs to be written. Hint: ```./CMake_XCode.sh``` in the root of the repository with XCode and CMake installed and then open the generated project    

---

## Clone the repository (for all platforms)

Before we can build the engine, we need to clone the repository recursively. To do this, you must have `git` installed.  To clone the repository, use this command:
 
```
git clone --recursive https://github.com/AtomicGameEngine/AtomicGameEngine
```
## Setting up a development build with Visual Studio

**Install Visual Studio**

You may either install Visual Studio 2015 or 2017 - they can both be downloaded from [https://www.visualstudio.com/downloads/](https://www.visualstudio.com/downloads/).   

- Make sure to install the C++ compiler and optionally the C# tools and Xamarin for mobile development

- For VS2015, you also need CMake installed which can be downloaded from [https://cmake.org/download/](https://cmake.org/download/). In the installer, make sure you allow it to add itself to `PATH`

**Configure the project**

Open `cmd` and change directory to the root of the repository you just pulled down:
```
cd C:\Users\User\path_to_repository
```
Now, type in `CMake_VS2015.bat` or `CMake_VS2017.bat` depending on the version of Visual Studio you have. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/cmdCommands.png" vspace="10"> <br/>
When it's finished, it'll have created a new folder for you in the same location as the root repository folder. From here, open `Atomic.sln` with Visual Studio. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/folderLayout.png" vspace="10"> <br/>
Once the solution is prepared for you, set `AtomicEditor` as the startup project. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/vsStartupProject.png" vspace="10"> <br/>
Build the solution by navigating to `Build > Build Solution` or using `F6`. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/vsBuildSolution.png" vspace="10"> <br/>

**Start debugging**  
Once the build is complete, open the source file you want to debug and set a *breakpoint* by clicking on a line of code in the left gutter.  
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/vsStartDebugging.png" vspace="10"> <br/>
Now, you can start debugging the project by navigating to `Debug > Start Debugging` or using `F5`.  
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/vsRunDebug.png" vspace="10">  
Visual Studio should now start debugging the engine. Once the breakpoint has been hit, you'll be able to inspect variables and view the callstack.  
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/vsVariablesCallstack.png" vspace="10">  

**Debugging a C++ Atomic Project**

You can also debug an Atomic project by launching the editor in player mode. To do so, navigate to the project properties and add the `--player` and `--project` flag followed by the absolute path to your project. Ensure that you have loaded the project at least once into the editor first, to generate the cache. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/vsAddArgument.png" vspace="10">

---

## Setting up a development build with QtCreator

These instructions can be used to setup a development build on macOS and Linux.

**Install QtCreator**  
First, install QtCreator, which can be downloaded from [https://www.qt.io/download/](https://www.qt.io/download/). (You may install it without a runtime, since it won't be necessary in this case)

**Configure the project**

Open QtCreator and select `File > Open File or Project`. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/qtOpenProject.png" vspace="10"> <br/>
Navigate to the root of the repository you just pulled down, and open `CMakeLists.txt`. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/qtSelectCMakeLists.png" vspace="10"> <br/>
At this point, leave everything as-is and click `Configure Project`. <br/>
Wait until QtCreator is finished setting up the project for you. Now, in order to speed up compilation time, navigate to `Project > Build Steps` and add the `-j4` argument. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/qtAddArgument.gif" vspace="10"> <br/>
**Start debugging** <br/>
Select `Debug` and choose `Atomic Editor` in the lower left of QtCreator. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/qtDebugMode.png" vspace="10"> <br/>
Build the project by navigating to `Build > Build All` or using `CMD`/`CTRL + SHIFT + B`.
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/qtBuild.png" vspace="10"> <br/>
Navigate to whatever file you'd like to debug and create a *breakpoint* by clicking a line of code in the left gutter.
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/qtBreakpoint.png" vspace="10"> <br/>
Finally, you can start debugging the engine by navigating to `Debug > Start Debugging` or using `F5`. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/qtStartDebugging.png" vspace="10"> <br/>
QTCreator should start debugging for you, and once the breakpoint has been hit, you will be able to inspect variables and view the callstack. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/qtVariablesCallstack.png" vspace="10">

**Debugging a C++ Atomic Project**

You can also debug an Atomic project by launching the editor in player mode. To do so, select the `Project` tab on the left and under the `Run` settings, add the `--player` and `--project` flag followed by the absolute path to your project. Ensure that you have loaded the project at least once into the editor first, to generate the cache. <br/>
<img src="images/repo/setting-up-atomic/how-to-set-up-a-debug-development-build/qtRunSettings.png" vspace="10"> <br/>

