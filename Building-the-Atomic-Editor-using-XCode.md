### Prerequisites 

The Atomic Editor is very easy to build and requires minimal setup.  You'll need XCode from the App Store and CMake: [http://www.cmake.org/download](http://www.cmake.org/download)

### Step 1: Clone and generate XCode project

Open a terminal and clone the Atomic Game Engine source and examples repositories.

    git clone --recursive https://github.com/AtomicGameEngine/AtomicGameEngine
    git clone https://github.com/AtomicGameEngine/AtomicExamples

Make a build directory, change directory to it, and run cmake.

    mkdir AtomicGameEngine-build
    cd AtomicGameEngine-build
    cmake ../AtomicGameEngine -G "Xcode"

This will generate an XCode project which you can now open.

[[images/build/xcode/mkdir_cd_cmake_atomicgameengine.png]]

### Step 2: Build the XCode project

In Finder, navigate to your build folder and open Atomic.xcodeproj

[[images/build/xcode/open_xcode_project.png]]

Once opened, select Build for Running from the Product menu

[[images/build/xcode/xcode_build_for_running.png]]

Once built, select the Atomic Editor as the current project

[[images/build/xcode/set_atomic_editor_current_project.png]]

### Step 3: Run the Editor

Run the editor using cmd-R or selecting Run from the Product menu
[[images/build/xcode/run_editor.png]]

### Step 4: Open the Physics Platformer example

From the Welcome Screen, select **Open Project** and navigate to your clone of the Atomic Examples, choose the Physics Platformer example

[[images/build/xcode/open_physics_platformer.png]]

The Platformer will open in the editor

[[images/build/xcode/physicsplatformer_open.png]]

### Step 5: Press Play!

You are now ready to start developing using your custom build of the Atomic Editor, press play!

[[images/build/xcode/physicsplatformer_playing.png]]
