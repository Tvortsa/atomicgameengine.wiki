Back to [Getting Started](Getting-Started)

![](images/repo/Getting_Started/the-atomic-editor/header.png)

### Introduction

Atomic Editor binary releases are managed by THUNDERBEAST GAMES LLC for the Windows, macOS, Android, iOS, and WebGL platforms.  Linux currently requires [compiling from source](Building-Atomic-from-Source), however we will provide binaries in the future.  

### Download

Download and install the latest Atomic Editor release for your platform at: [http://atomicgameengine.com/#download-the-atomic-editor](http://atomicgameengine.com/#download-the-atomic-editor)

Atomic Editor releases are scheduled as rolling [milestones.](https://github.com/AtomicGameEngine/AtomicGameEngine/milestones)  If you are interested in contributing code, art, documentation, examples, or anything else, awesome 👍 , please see: [How to help make Atomic better.](How-to-help-make-Atomic-better)

**C# Support <sup>Optional</sup>**

The Atomic Editor integrates with [Visual Studio](https://www.visualstudio.com/vs/community/), [Visual Studio for Mac](https://www.visualstudio.com/vs/visual-studio-mac/), and [MonoDevelop](Detailed-instructions-for-building-on-Linux) to provide a first class editing, debugging, and deployment experience on desktop and mobile platforms.  Install the IDE for your platform to enable C# scripting support.

#### License Agreement & Registration
<img align="right" src="images/repo/setting-up-atomic/atomicregister.gif">

On the initial run, the editor will confirm licensing terms and provide registration for a new Atomic Community ID or to enter an existing one.

During registration you can optionally sign up for updates and anonymous editor usage metrics. Registration is a spam free zone with no information being shared and only notification of significant updates.

#### Getting Started

Please see these [getting started](Getting-Started) topics to become familiar with the basics of the Atomic Editor.

#### Getting Help

The Atomic Community is comprised of veteran professionals and those just starting out.  If you're stuck, check the community site and chat, chances are someone has had a similar issue or will be able to help out.

<a href="https://discourse.atomicgameengine.com/" target="_blank">![](http://atomicgameengine.com/images/community/CommunitySupportPortal.png)</a>
<a href="https://gitter.im/AtomicGameEngine/AtomicGameEngine/" target="_blank">![](http://atomicgameengine.com/images/community/Community_Chat.png)</a>
