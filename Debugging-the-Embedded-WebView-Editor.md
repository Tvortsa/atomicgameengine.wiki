[Back to Cookbook](Atomic-Cookbook)

The embedded webview editor can be debugged by using an external instance of the Google Chrome browser or Chromium that can attach to the Chrome dev tools inside the embedded web view.  

Launch Chrome and navigate to the following URL: **http://localhost:3335/**  

This will give you a list of editor tabs that you can debug.  You should be able to use the typical features to inspect variables, set breakpoints, etc.