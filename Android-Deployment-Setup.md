### Android Deployment – Setup

The following steps describe how to setup the development environment for Android deployment.

---

#### 1. Install Java JDK

Download and install the latest Java JDK from Oracle: [[http://www.oracle.com/technetwork/java/javase/downloads/index.html]]

_To check if the JDK is already installed, open a command prompt (windows) or terminal (mac) and type `java -version`_
 
---

#### 2. Install Android SDK

* Download and install the latest Android SDK: [[http://developer.android.com/sdk/index.html]]
* Follow the instructions under “Installing the SDK”

---

#### 3. Download Apache Ant

Download Apache Ant: [[http://ant.apache.org/bindownload.cgi]]

---

#### 4. Install Device Drivers

Connect the Android device via USB.

_On Windows_: If the device is not recognized automatically, visit the manufacturer's website and download/install the latest drivers.

_On Mac_: No additional drivers should be required.

---

Next Up: [Building From the Editor](Building-From-the-Editor)