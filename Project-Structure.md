Back to [Getting Started](Getting-Started)

![](images/repo/Getting_Started/project-structure/header.png)

# Project Structure

When making a new project in the Atomic Game Engine, a project folder is created for you at a specified location on your hard drive. The root of the folder looks like this:

<img align="left" src="images/repo/Getting_Started/project-structure/folderIcon.png" height="17">• Cache<br/>
<img align="left" src="images/repo/Getting_Started/project-structure/folderIcon.png" height="17">• Resources<br/>
<img align="left" src="images/repo/Getting_Started/project-structure/folderIcon.png" height="17">• Settings<br/>
<img align="left" src="images/repo/Getting_Started/project-structure/folderIcon.png" height="17">• Cache<br/>
<img align="left" src="images/repo/Getting_Started/project-structure/fileIcon.png" height="17" vspace="3" hspace="1">• [ProjectName.atomic]<br/>

If you have C# enabled in your project, you will have these folders as well:

<img align="left" src="images/repo/Getting_Started/project-structure/folderIcon.png" height="17">• AtomicNET<br/>
<img align="left" src="images/repo/Getting_Started/project-structure/folderIcon.png" height="17">• Project<br/>

---

### Cache

In this folder, Atomic generates cache files for your project that the Editor uses - with scripting, you can also access your project files via the cache. The files in here are all generated automatically and you won't need to do anything in here manually.

---

### Resources

This folder contains all of the scenes, models, textures, sprites, materials, scripts and components that you have imported and created for your project. If you import anything into your project via the Editor, this is where it lands. You can also create new folders and copy files into here, and the Editor will pick it up. Typically, this is where you would store all files for your game, therefore making it a crucial folder for your project.

---

### Settings

This is where the settings for your project and application are stored. As of now, two kinds of files can be put in here for the Atomic Game Engine to use. They're called Project.json and Settings.json.
You can read more about these files by visiting their respective pages on this wiki.

---

### [ProjectName].atomic

This is the file that opens your project. You can click on this manually in the root folder to open Atomic, or you can open it inside of Atomic with `File - Open Project`.

---

### AtomicNET

In this folder, your C# solution and other related files are stored. These are automatically-generated files - and usually, you won't be spending too much time in here. However, if for some reason Atomic fails to correctly open your IDE, you can open the solution file in `AtomicNET > Solution > [ProjectName].sln`.

---

### Project

This folder contains the main C# scripts needed to run your application. Most of the time, you also won't be doing anything in here.