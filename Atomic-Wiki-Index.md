![](https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/indexHeader.png)

# The Index

Here, you'll find a list of all the goodness within the Wiki. They've been categorized for you, so it'll be easier to find. You can also use the search function on the right to find specific content. <br/>

If you're a new Atomic user, we recommend looking at the [Getting Started](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Getting-Started) pages.

---

### Setting up Atomic

| Title                                      | Description    |
| ------------------------------------------ | -------------- |
| [Building the Atomic Editor:<br/> The Quick and Easy Way](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Building-Atomic-from-Source)| Quick and easy instructions on building the Atomic Editor from source, anyone can do it! |
| [Detailed Instructions on<br/>building on Linux with C#](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Detailed-instructions-for-building-on-Linux) | Detailed instructions on how to build the engine from source, bundled with MonoDevelop for C#, in a Linux environment |
| [Developing with VS2015](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Building-the-Atomic-Editor-using-Visual-Studio-2015) | How to setup a development environment for native C++ using Visual Studio 2015
| [Developing with XCode](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Building-the-Atomic-Editor-using-XCode) | How to setup a development environment for native C++ using XCode |
| [Developing with QtCreator](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Building-the-Atomic-Editor-using-QtCreator) | How to setup a development environment for native C++ using QtCreator |
| [How to Debug Native Code](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/How-to-set-up-a-Debug-Development-Build) | Explains how to set up a debug development build on Linux, macOS & Windows using QtCreator or Visual Studio. |
| [Performance and Memory Profiling](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Profiling) | Explains how to use Atomic's built-in profiling and object tracking |

### Scripting

| Title                                      | Description    |
| ------------------------------------------ | -------------- |
| [Using TypeScript with<br/> External Editors](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/Using-TypeScript-with-External-Editors)| Instructions on how to script with TypeScript using Atom & VSCode |
| [Basic Scripting in C#](Basic-Scripting-in-CSharp) | Simple programming instructions for those just getting into Atomic and C# |