#Blender to Atomic Game Engine Mesh Exporter  
io_mesh_atomic v0.6 (forked from io_mesh_urho v0.6)

This is a Blender addon that helps you exporting your meshes, armatures, animations and materials to the Atomic Game Engine :camel:. It has identical function to the io_mesh_urho exporter, except it outputs files by Atomic resource type (.material, .scene, .prefab) instead of .xml and internal names have been changed so both the atomic and urho3d exporters can be installed in blender at the same time. It is an amateur work, done with little knowledge of the Atomic Game Engine, Urho3D and Blender, but I hope it helps.

Public domain license CC0. The Urho3D version is maintained by reattiva, the Atomic Game Engine version is maintained by JimMarlowe. 

## Installation from releases
- download the latest release zip file https://github.com/JimMarlowe/Urho3D-Blender/files/791512/Atomic-Blender.zip
- unzip it;
- select the folder 'io_mesh_atomic' and re-create a zip file (you need the folder 'io_mesh_atomic' in the zip file);
+ open Blender;
- open 'user preferences' (Ctrl+Alt+U);
- click 'Addons' on the top bar;
- click 'Install from file...' on the bottom bar;
- select the zip file;
- now search in the list, you should see a greyed line named 'Import-Export: Atomic export';
- click on the checkbox on the right to enable the addon (see below if you can't set the check);
- optional: click 'Save User Settings' to remember this addon as enabled.

Detailed usage information is available in this guide https://github.com/JimMarlowe/Urho3D-Blender/blob/JM-ATOMIC-EXPORTER/guide.txt
