Back to [Getting Started](Getting-Started)

![](https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/Getting_Started/components/header.png)

# Components

**Components** are attachments to a [node](node). There are many different types of components, and they all have a variety of modifiable values that can be altered within the Atomic Editor or via scripting.<br/>
You can read more about components on the [Urho3D wiki](https://urho3d.github.io/documentation/1.3/_scene_model.html).

Noticeable Components used often are Node, StaticModel and JS/CSComponent.

---

### Creating new components
<img src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/Getting_Started/components/componentExamples.png" align="right" height="449"/>

<img src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/Getting_Started/components/createComponentMenu.png" align="left" vspace="11"/>
**In the [Atomic Editor](the atomic editor)**</br>

Components are exposed to the Atomic Editor within the inspector (see picture on the right). The inspector contains all of the components currently attached to the selected Node.

From within the inspector, you can click on `Create Component` to add a component to the selected node. You will be granted with a new menu, where you can select what kind of component you want to add (see picture on the left).

**With scripting**</br>

JavaScript:</br>
```
node.createComponent("Insert component here");
```

C#:</br>
```
Node.CreateComponent<Insert component here>();
```

---

### Modifying components

Some of a Component's values can be modified within the Atomic Editor, and can be done within the inspector (see first picture on the right).
However, to gain full access to a component, you will need to do so with scripting. Below is a code example where we access a RigidBody component, and use it to apply some impulse to a node.

JavaScript:
```
// Accessing the RigidBody component, which is already created in the editor
var rigidbody = self.node.getComponent("RigidBody");

// Apply impulse to the RigidBody
rigidbody.applyImpulse([0, 0, .1]);
```

C#:
```
// Accessing the RigidBody component, which is already created in the editor
var rigidbody = Node.GetComponent<RigidBody> ();

// Applying impulse to the RigidBody
rigidbody.ApplyImpulse(new Vector3(0f, 0f, .1f));
```