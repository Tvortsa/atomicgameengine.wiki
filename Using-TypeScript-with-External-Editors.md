![](https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/scripting/using-typescript-with-external-editors/header.png)
### Using TypeScript with External Editors

---

Instead of the built-in Monaco editor, you may use an external editor. In this overview, we'll explain how to use Atom and VSCode.

## Setting up a New Project
* Create a new TypeScript project in the Atomic Editor
* Navigate to `Developer > Plugins > TypeScript > Create External Editor Project` <br/>
<img src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/scripting/using-typescript-with-external-editors/generateExternalProject.png" vspace="12.5"> <br/>
This will generate a `tsconfig.json` file and copy over the latest `Atomic.d.ts` into your project directory.

## Using Atom as an External Editor
First, make sure that you have a copy of Atom installed - it can be found @ [https://atom.io](https://atom.io) and is available for all major platforms. <br/>

<img src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/scripting/using-typescript-with-external-editors/atomPackage.png" align="left" vspace="10">
Besides Atom, you will also need the [Typescript package](https://atom.io/packages/atom-typescript). You can install it manually or use the built-in package installer to install the package (see picture to the left). <br/><br/>
When all these steps are done, open your root project folder inside of Atom, and voila, you should be good to go. To build the project, use `F6`. <br/>

Additionally, you can use a linter to help identify some style and best practice violations in your code:<br/>
[https://atom.io/packages/linter](https://atom.io/packages/linter)<br/>
[https://atom.io/packages/linter-tslint](https://atom.io/packages/linter-tslint)  

## Using VSCode as an External Editor
First, make sure you have a copy of VSCode installed - it can be found @ [https://code.visualstudio.com](https://code.visualstudio.com), and is, just as Atom, available on all major platforms. <br/>

In VSCode, open the root of your project folder. Now, you will need to configure a Task Runner - you can do this by opening the Command Palette (`View > Command Palette` or `CMD`/`CTRL + SHIFT + P`) and finding `Tasks: Configure Task Runner`.<br/>
<img src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/scripting/using-typescript-with-external-editors/taskRunner.png" vspace="12.5"><br/>
Select `TypeScript: tsconfig.json` in the task runners available. Now, you should be able to edit your TypeScript and add new scripts inside of VSCode. To Build, press `CMD`/`CTRL + SHIFT + B`.<br/>

<img src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/warning.png" align="left" width="25"> If you're on Linux, you may have to install TypeScript using `sudo npm install -g typescript` first.

Additionally, you can use this [linter](https://marketplace.visualstudio.com/items?itemName=eg2.tslint) to help identify some style and best practice violations in your code.

