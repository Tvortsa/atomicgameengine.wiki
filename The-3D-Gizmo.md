Back to [Getting Started](Getting-Started)

![](images/repo/Getting_Started/the-3d-gizmo/header.png)

# The 3D Gizmo

The gizmo is used to manipulate nodes in your scene. It will appear when selecting a node. <br/>
The [Atomic Editor](the-atomic-editor) has 3 kinds of gizmos:

<img src="images/repo/Getting_Started/the-3d-gizmo/moveGizmo.png" align="left" height="50">
1) **The move gizmo**<br/>
Used to move the selected object.

<img src="images/repo/Getting_Started/the-3d-gizmo/scaleGizmo.png" align="left" height="46">
2) **The scale gizmo**<br/>
Used to scale the selected object.

<img src="images/repo/Getting_Started/the-3d-gizmo/rotateGizmo.png" align="left" height="46">
3) **The rotate gizmo**<br/>
Used to rotate the selected object.

</br>
By dragging one of the 3 sides of the gizmo, you will manipulate the selected node in its corresponding direction by its respective function (move, translate, or scale).</br>

Additionally, you can also move multiple axes in the gizmo if you hover the mouse over the middle of the three axes, demonstrated below:

<img src="images/repo/Getting_Started/the-3d-gizmo/demonstration.gif">