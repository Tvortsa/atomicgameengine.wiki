![](https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/Getting_Started/gettingStarted_header.png)

### Getting Started

Если вы только начинаете знакомиться с Atomic то это место для вас.  

Мы собрали вместе все страницы wiki нужные новичку в Atomic.

---

Название             | Описание                       |
------------------| --------------------------------- |
[Настройка редактора Atomic](Setting-up-the-Atomic-Editor) | How to setup the Atomic Editor for your platform |
[Atomic Editor Overview](The-Atomic-Editor) | Обзор редактора Atomic |
[Scene Editor Hotkeys](Scene-Editor-Hotkeys) | Горячие клавиши в scene editor  |
[Структура проекта](Project-Structure) | We take a look at how a project is structured and what every folder contains |
[Узлы](Node) | Explains what nodes are  |
[Компоненты](components) | Explains what components are, how to add & modify them |
[3D Гизмо](The-3D-Gizmo) | Describes the three gizmos used in the Atomic Editor |
[Карты высот] (heightmaps) | How heightmaps work and how they are imported into the engine |
[Материалы](Basic-Materials) | How to add textures to your 3D models |
[2D анимация в Spriter Pro](2D-Animations-with-Spriter-Pro) | Adding animations to your project via Spriter Pro projects |
[Скриптинг в C#](Basic-Scripting-in-CSharp) | Shows how to move a node using C# scripting |