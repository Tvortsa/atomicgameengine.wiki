![](images/repo/contributing-to-the-wiki/contributingHeader.png)

##Instructions for contributing to the Atomic Wiki
This wiki is *the* place to document all things Atomic.  If you would like to contribute a documentation topic, please look over the instructions below.  Existing article markdown is a good source, especially if you see a particular layout you like on an existing page... alright, let's get to it, read on!

###First of all...
Before attempting to write anything, we encourage you to check out other articles on the wiki, just to get a taste of how they're set up. If possible, we'd love to maintain the same kind of "layout" throughout the wiki, so it doesn't turn into a big mess. But fret not, we'll go over setting up a new page on the wiki in this article!

####Text formatting
GitHub wikis, like this one, use `markdown` to format text. When writing articles, you're going to have to use it - you can read more about it [here](https://guides.github.com/features/mastering-markdown/).

####Header
Most pages on the wiki start off with a graphical banner that represents the article. This article has one, take a look. You don't have to make one of these, but if you feel adventurous, feel free to do so. The **template** can be found here:

[Download as `.xcf`](https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/wikiHeaderTemplate.xcf) (GIMP)

[Download as `.pdn`](https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/wikiHeaderTemplate.pdn) (paint.net)

[Download as `.png`](https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/wikiHeaderTemplate.png) (standard PNG)

###Start writing!

####Title and description
Before getting into the nitty-gritty details, take a minute to describe what you're trying to demonstrate. Start off with a title that describes your article in one sentence. After that, take some time to quickly go over what the reader is going to learn when reading your article.

####Get into details
Time to let it all out! We can't help you much here, other than - keep the language simple and clean, use correct grammar when possible and describe all the little details. 

####Inserting links
You can refer to all pages on the world wide web - but if you just want to link to another wiki article here, you can use: "`[insert text](Building-Atomic-from-Source)`" - and this would be the result: [insert text](Building-Atomic-from-Source).

To figure out what to write inside the parentheses, check out the omnibar in your browser:

<img src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/contributing-to-the-wiki/omnibar.png">

####Inserting pictures
To make everyone happy, we would much prefer if pictures are stored in our wiki repository instead of linking to them externally. You can pull down the repository by using:
```
git clone https://github.com/AtomicGameEngine/AtomicGameEngine.wiki.git
```
In there, navigate to `images > repo`. Everything in here should be somehow organized into sections, and you can probably figure out where to put your own pictures. When finished, commit your changes and push 'em up. After that, you can use the pictures in your article - either like this: `[[images/repo/picture.png]]` or by using HTML. 

You may use *HTML* if you want a picture aligned differently, want to include some vertical or horizontal space, or change width and height. When using HTML, you have to provide the full link to your picture. Here's an example:
```
<img src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/picture.png" align="left" vspace="10">
```
This could look something like *this*:

<img src="https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/contributing-to-the-wiki/alignedPicture.png" align="left" vspace="10" width="200">

That picture surely is aligned to the left!

It can make your article look a bit more interesting.

It may take a bit more time to set up, but it usually pays off ;)
<br/><br/><br/><br/><br/>

**End it with a summary** <br/>
A good article sums up what has just been demonstrated. In various cases, this isn't really necessary, but sometimes it can make your article look a bit more professional. A tutorial might benefit from it, for example.

### Summary
See what I did there? Maybe you should do it as well. Very well, this has been the basics of setting up an article on the wiki. You might have questions, and if you do, you can usually catch someone on the [gitter chat](https://gitter.im/AtomicGameEngine/AtomicGameEngine). 

Thanks for your interest in contributing to the wiki! We much appreciate your help!