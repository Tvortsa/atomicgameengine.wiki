Atomic features realtime performance and memory metrics on desktop and mobile devices. Profiling can be selected from the Atomic Editor Tools menu, enabled in code, or specified on the command line.

Once enabled in the Atomic Editor, you will see profiling data for the editor itself.  While profiling is on, player sessions will also display profiling data by default.

[[images/repo/profiler/ToolProfilerMenu.jpg]]

## Performance Profiling

Profiling performance gives a hierarchical breakdown of individual engine methods

[[images/repo/profiler/ProfilePerformance.jpg]]

> Hierarchical application performance profiling of a basic 3d scene

## Memory Metrics

Atomic has robust object tracking to help catch object leaks and related memory issues. The Metrics subsystem also tracks whether native, C#, or JavaScript/TypeScript code instantiated objects.  Metrics tracking is selected in the Atomic Editor Tools menu in the Profiler submenu.

#### Object Tracking

[[images/repo/profiler/ProfileMetrics.jpg]]

> When memory metrics are enabled, objects are tracked and instance counts can be displayed

#### Enabling metrics from code

Example C# to enable metrics subsystem

```CSharp
// Objects instances created from this point will be tracked
GetSubsystem<Metrics>().Enable();

// Show the hud
var ui = GetSubsystem<UI>();
ui.ShowDebugHud(true);
ui.SetDebugHudProfileMode(DebugHudProfileMode.DEBUG_HUD_PROFILE_METRICS);
```
#### Autometrics and Player Metrics (Advanced)

The ```--autometrics``` command line option can be specified for both the Atomic Editor and player applications.  This will automatically enable the Metrics subsystem and begin tracking objects starting with the Engine subsystem.  When running the Atomic Editor with autometrics enabled, player sessions will also have autometrics enabled.

The ```--playermetrics``` command line option can be specified for player applications.  This will automatically enable the Metrics subsystem and begin tracking objects starting at the beginning of application code (after the engine subsystems are created).

[[images/repo/profiler/deepmetrics.jpg]]

> When autometrics are enabled, object tracking begins with the Engine subsystem for deep tracking of core objects

Example C# of setting autometrics from code

```CSharp
public class Program
{
    public static void Main(string[] args)
    {
        // Capture all metrics, including engine subsystems, etc
        // Autometrics must be set before the Application instance is created
        Application.SetAutoMetrics(true);

        Application.Run<AtomicMain>(args);
    }
}
```

Metrics command line options also work when launching from Visual Studio

[[images/repo/profiler/autometrics.jpg]]

> Example of adding --autometrics to Visual Studio C# command line options to track all engine objects
