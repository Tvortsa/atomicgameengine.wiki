
##Introduction

Here are some articles for developers working on the Atomic Game Engine itself.  This is (clearly) a work in progress.  If something seems missing, please consider adding it.

### Quick Links

- [General Development](#general-development)
- [JavaScript/TypeScript Developers](#javascripttypescript-developers)
- [C# Developers](#c-developers)  
- [C++ Developers](#c-developers-1)


#### General Development

General information relevant to all implementation languages.

Title             | Description                       |
------------------| --------------------------------- |
[**Building Atomic from Source**](Building-Atomic-from-Source)| It is easy to build Atomic from source, these are the instructions |

#### JavaScript/TypeScript Developers

Information for working on Atomic in JavaScript and TypeScript

Title             | Description                       |
------------------| --------------------------------- |
**[Atomic Editor TypeScript dev setup](Atomic-Editor-TypeScript-Development-Setup)** | Setting up a TypeScript environment to work on the Atomic Editor |
**JavaScript Code Conventions** | To Be Written |
**TypeScript Code Conventions** | To Be Written |

#### C# Developers

Information for working on Atomic in C#

Title             | Description                       |
------------------| --------------------------------- |
**C# Code Conventions** | To Be Written |

#### C++ Developers

Information for working on Atomic in C++ or debugging native code.

Title             | Description                       |
------------------| --------------------------------- |
[**C++ Code Conventions**](Coding-Conventions-(CPlusPlus)) | Coding conventions for C++ developers |
[**Visual Studio Setup**](How-to-set-up-a-Debug-Development-Build)| How to setup Visual Studio for Atomic C++ development |
[**QtCreator Setup**](How-to-set-up-a-Debug-Development-Build)| How to setup QtCreator for Atomic C++ development on macOS and Linux |
**XCode Setup**| To Be Written |

