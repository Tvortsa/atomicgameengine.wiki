### Prerequisites 

The Atomic Editor is very easy to build and requires minimal setup.  You'll need [QtCreator](https://www.qt.io/download-open-source) and CMake: [http://www.cmake.org/download](http://www.cmake.org/download)

### Step 1: Clone the Atomic Game Engine and Examples

Open a terminal and clone the Atomic Game Engine source and examples repositories.

    git clone --recursive https://github.com/AtomicGameEngine/AtomicGameEngine
    git clone https://github.com/AtomicGameEngine/AtomicExamples

[[images/build/qtcreator/git_clone.png]]

### Step 2: Open the Atomic Game Engine project in QtCreator and Build

QtCreator supports CMake files as native projects.  In QtCreator select Open Project and then navigate to the CMakeLists.txt in the root of the local Atomic Game Engine repository

[[images/build/qtcreator/qtcreator_open_cmake.png]]

Once opened, you'll be asked for some basic CMake configuration information, leave the defaults, and run CMake

[[images/build/qtcreator/qtcreator_run_cmake.png]]

On the Projects tab, on Build Steps, add -j8 to the make arguments to enable using multiple core compilation

[[images/build/qtcreator/qtcreator_enable_cores.png]]

From the Build menu, select Build All

[[images/build/qtcreator/qtcreator_buildall.png]]

On the Project tabs, select Run and then ensure that the AtomicEditor is set as the Run configuration

[[images/build/qtcreator/qtcreator_set_atomiceditor_run.png]]

### Step 3: Run the Editor

Run the editor using cmd-R or selecting Run from the Build menu
[[images/build/qtcreator/qtcreator_run.png]]

### Step 4: Open the Physics Platformer example

From the Welcome Screen, select **Open Project** and navigate to your clone of the Atomic Examples, choose the Physics Platformer example

[[images/build/qtcreator/open_physics_platformer.png]]

The Platformer will open in the editor

[[images/build/qtcreator/physicsplatformer_open.png]]

### Step 5: Press Play!

You are now ready to start developing using your custom build of the Atomic Editor, press play!

[[images/build/qtcreator/physicsplatformer_playing.png]]
