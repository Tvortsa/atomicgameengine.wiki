[[images/repo/cookbook/cookbook.png]]

# The Atomic Cookbook

Here, you'll find a list of recipes for getting things done with Atomic. This includes features currently in development, solutions to problems off the beaten path, and information that hasn't been surfaced in other resources, but can be valuable to artists and developers alike.<br/>  

### Scripting

- [Using VSCode and Atom for TypeScript instead of built in code editor](Using-TypeScript-with-External-Editors)
- [Changing the Code Editor's Display Settings](Changing-the-Code-Editor's-Display-Settings)
- [Debugging the Embedded WebView Editor](Debugging-the-Embedded-WebView-Editor)

### Assets

- [Changing how models are imported](Changing-how-models-are-imported)

## Atomic Game Engine Development

These recipes are for working on the Atomic Game Engine itself.

### Atomic Editor

- [TypeScript Development Setup](Atomic-Editor-TypeScript-Development-Setup)

