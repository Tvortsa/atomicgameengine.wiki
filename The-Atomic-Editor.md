назад к [Getting Started](Getting-Started)

![](images/repo/Getting_Started/the-atomic-editor/header.png)
### Введение

Редактор Atomic это одна из ключевых функций ядра Atomic Game Engine, он является визуальным интерфейсом для изменения вашего проекта. Редактор включен во все установки Atomic Game Engine, и в нем вы проводите большинство времени.

Открыв редактор, он будет выглядеть как: (*example projects* отобразятся только если были установлены)
<br/>
<br/>
![](images/repo/Getting_Started/the-atomic-editor/introduction.png)

---
### Создание нового проекта
<img align="left" src="images/repo/Getting_Started/the-atomic-editor/createProject1.png" width="300">
**Для создания нового проекта**, нажмите New Project вверху редактора, или перейдите на `File > New Project`.

Теперь, теперь выберите тип проекта.<br/>
Типы проектов:

&nbsp;• 2D    | Стандартный 2D проект.<br/>
&nbsp;• 3D    | Стандартный 3D проект.<br/>
&nbsp;• Empty | Создает пустой проект для сомостоятельного конфигурирования.<br/>
<br/><br/>
<img align="left" src="images/repo/Getting_Started/the-atomic-editor/createProject2.png" width="311">
Выбрав тип проекта, вы попадете в новое окно. Здесь задается имя проекта, ваш App ID (если есть), выберите целевую платформу, папку проекта, и язык скриптинга который вы хотите использовать.<br/>

Выбор языка скриптинга не не означает что вы не сможете использовать другие языки в проекте. For example, you can create a C# project and use .NET features while also implementing JavaScript in other aspects of your project.

---

### Открытие проекта
При открытии нового проекта, (также через `File > Open Project`, выберите проект, или через [ProjectName].atomic),  Atomic Editor подготовит его для вас - что будет выглядеть примерно так:
<br/><br/>
<img src="images/repo/Getting_Started/the-atomic-editor/openProject.png">
<br/><br/>
Есть *четыре* основных раздела:<br/><br/>
<img align="left" src="images/repo/Getting_Started/the-atomic-editor/projectSection.png">**Раздел проекта**<br>
Раздел проекта отображает корневую папку для всего содержимого проекта. Здесь вы оперируете всеми файлами и папками.

Нажимая кнопку `Create`, вы получите опции для создания различных сущностей вашего проекта:<br/>
• A folder<br/>
• Component<br/>
• Script<br/>
• Material<br/>
• Scene<br/>
вы также можете отыскать все ваши ресурсы используя строку поиска справа.
<br/><br/><br/><br/>
<img align="left" src="images/repo/Getting_Started/the-atomic-editor/hierarchy.png">**Hierarchy**<br>
Здесь отображаются все узлы вашей сцены. На картинке показано слева, нет ни одного узла так как никакая сцена не была загружена в редактор. Но когда мы загрузим сцену, здесь отобразятся узлы.<br/>

Кликая кнопку Create (при загруженной сцене), вы уидите опции создания нового пустого узла или источника света.<br/>

Hierarchy так же то место в котором задают родителей.<br/><br/><br/>

<img align="left" src="images/repo/Getting_Started/the-atomic-editor/3dview.png" width="310">**3D view**<br/>Открывая сцену в разделе Project (сцены обычно хранят в папке Scenes, и имеют расширение .scene ), она отобразится в 3D view.
 
 Вы можете летать вокруг 3D view нажав правую кнопку мыши и нажимая клавиши `W A S D` на клавиатуре (типичное управление от первого лица).
 
 Выделите узел левым кликом. Появится [3D gizmo](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/The-3D-Gizmo) , которое можно использовать для манипуляций узлом (перемещение,вращение, масштабирование).<br/><br/><br/>

<img align="left" src="images/repo/Getting_Started/the-atomic-editor/inspector.png">**Inspector**<br/>
Инспектор отображает все компоненты которые содержит выделенный узел. Вы можете узнать о компонентах больше -  [здесь](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/components## ).

В инспекторе, отображаются наиболее важные параметры присоединенных компонентов и их значения можно изменять.

Обычно, работая с компонентами, вы делаете это через скриптинг.



