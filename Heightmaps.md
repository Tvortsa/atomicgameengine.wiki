Back to [Getting Started](Getting-Started)

![](images/repo/Getting_Started/heightmaps/header.png)

# Heightmaps

---

A heightmap is an image that contains a series of black and white points, which Atomic can convert into a three-dimensional terrain that can be used for your games. This is how it works:

![](images/repo/Getting_Started/heightmaps/example.png)

The brighter points indicate where the terrain should be tall, and the darker points represent the lower parts of the terrain. In the example above, the white section is in the middle of a hill, making that section taller.</br>

Now, we'll take a look at how to import a heightmap in the [Atomic Editor](the-atomic-editor).

---

<img src="images/repo/Getting_Started/heightmaps/componentOverview.png" align="right" width="230">
<img src="images/repo/Getting_Started/heightmaps/selectScene.png" align="left" vspace="5">
**<- 1) Select your scene in the Hierarchy**<br/>
In the lower left corner (read ["The Atomic Editor"](the-atomic-editor) if you're in doubt), find the scene that you wish to create a terrain on.
<br/><br/>
<img src="images/repo/Getting_Started/heightmaps/createTerrainComponent.png" align="left" width="231" vspace="10">
<br/>**<- 2) Create a "Terrain" component**<br/>
In the inspector, you need to create a new component called "Terrain". It can be found under `Geometry > Terrain`. When it is created, the component options will look like what's shown to the picture on your right.
<br/><br/><br/><br/><br/><br/>
**3) Import your heightmap**<br/>
In the "Terrain" component options, the second option is the path to your heightmap. When clicking on the three dots besides the input field, you will be able to choose a picture in your Resources. Assuming you already have a heightmap imported somewhere in your resources, choose it here.
<br/><br/>
**4) Save your project**<br/>
Now, for Atomic to start generating your terrain, you need to reload your project. So go ahead and save your scene and your project, and reload it. Your terrain should be generated now.<br/><br/>
<img src="images/repo/Getting_Started/heightmaps/result.gif">
