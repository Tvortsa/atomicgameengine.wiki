### Running Projects Directly from the Atomic Editor 

It is now possible to run projects directly from the Atomic Editor using the --player and --project switches.

Internally, this is used for the new IPC mechanism which plays projects in a separate process.  However, it is useful here as under Visual Studio we can debug without having to do a build (or any packaging of files).  The --player and --project switches run the project directly from the project's source files.

Here's an example of setting up the option in VS, the --project switch gets the absolute path to the project

[[images/vscode/vsblog_atomiceditor_player.png]]

### VSCode Integration

Start VSCode and add your Atomic projects root folder, hit ctrl-shift-p and start typing "Tasks".  You should see Tasks:Configure Task Runner".  Select this and a ".settings/tasks.json" file should open with a bunch of placeholder stuff.

[[images/vscode/vsblog_controlshiftp_tasksconfigure.png]]

Replace the text with the following, changing the path to wherever you built AtomicEditor.exe:
```
// Runs an Atomic project
{
    "version": "0.1.0",
	
    // Path to the AtomicEditor
    "command": "C:/Dev/atomic/AtomicGameEngine-build/Source/AtomicEditor/Debug/AtomicEditor.exe",
			
    // We always want to see output		
    "showOutput": "always",
	
    "isShellCommand": true,
			
    // specify that we're running the player, the project, and to log to std out for VSCode
    "args": ["--player", "--project", "${workspaceRoot}", "--log-std"]
	
}
```

Open any source file and hit ctrl-shift-b

You should now be running your project with output going to the task output window as shown

[[images/vscode/vsblog_vscode_withoutput.png]]


