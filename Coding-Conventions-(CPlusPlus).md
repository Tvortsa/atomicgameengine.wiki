#### C++ Coding Conventions

An easy to digest coding convention is important when working with others and the list below are primary ingredients in helping keep Atomic's C++ code hot and spicy :)

> Copyright Notice: This article draws extensively from open source Urho3D project code convention documentation, however it has been modified by the Atomic Community

- **IMPORTANT:** Atomic was forked from Urho3D and we continue to pull C++ updates from master, so any modification to source files in ThirdParty or which is ```// Copyright (c) the Urho3D project```, please mark as described:

```cplusplus
// ATOMIC BEGIN
// A short comment about the change
// possibly linking to a GitHub issue

String variable = "example modification";

// ATOMIC END  
```

- It is also important to group additional API methods, member variables, of existing classes between ```ATOMIC``` blocks in these source files.  This makes merging *much* easier

- Indent style is Allman (BSD) -like, ie. brace on the next line from a control statement, indented on the same level. In switch-case statements the cases are on the same indent level as the switch statement.

- Indents use 4 spaces instead of tabs. Indents on empty lines should not be kept.

- Class and struct names are in camelcase beginning with an uppercase letter. They should be nouns. For example ```DebugRenderer, FreeTypeLibrary, Graphics```.

- Functions are likewise in upper-camelcase. For example ```CreateComponent, SetLinearRestThreshold```.

- Variables are in lower-camelcase. Member variables have an underscore appended. For example ```numContacts, randomSeed_```.

- Constants and enumerations are in uppercase. For example ```Vector3::ZERO or PASS_SHADOW```.

- Pointers and references append the * or & symbol to the type without a space in between. For example ```Drawable* drawable, Serializer& dest```.

- Class definitions proceed in the following order:
  - public constructors and the destructor
  - public virtual functions
  - public non-virtual member functions
  - public static functions
  - public member variables
  - public static variables
  - repeat all of the above in order for protected definitions, and finally private

- Header files are commented using one-line comments beginning with /// to mark them, comments are important as they will appear in script API references

- Inline functions are defined inside the class definitions where possible, without using the inline keyword.
