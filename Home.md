[[images/repo/homeHeader.png]]
### Atomic Game Engine Wiki

Title             | Description                       |
------------------| --------------------------------- |
[**Setting up the Atomic Editor**](Setting-up-the-Atomic-Editor) | How to setup the Atomic Editor for your platform |
[**Getting Started**](Getting-Started) | A list of pages geared towards new Atomic users |
[**Building Atomic from Source**](Building-Atomic-from-Source)| Instructions on building the Atomic from source, anyone can do it! |
[**Atomic Developers**](Atomic-Developers) | Information for developers working on the Atomic Game Engine|
[**Atomic Cookbook**](Atomic-Cookbook) | Recipes for getting things done with Atomic |
[**Atomic FAQ**](FAQ) | Have questions? Check to see if your question has been answered already |
[**Contributing to the Wiki**](Contributing-to-the-Wiki) | Instructions for contributing to the Wiki |
[**How to help make Atomic better**](How-to-help-make-Atomic-better) | Information on contributing code, art, documentation, examples, or anything else |
[**Full Topic Index**](Atomic-Wiki-Index) | A categorized list of topics within the Wiki |
### Additional Resources

#### Community

<a href="https://discourse.atomicgameengine.com/" target="_blank">![](http://atomicgameengine.com/images/community/CommunitySupportPortal.png)</a>
<a href="https://gitter.im/AtomicGameEngine/AtomicGameEngine/" target="_blank">![](http://atomicgameengine.com/images/community/Community_Chat.png)</a>
<a href="http://atomicgameengine.com/blog/" target="_blank">![](http://atomicgameengine.com/images/community/Community_Blog.png)</a>

#### API References

<a href="http://docs.atomicgameengine.com/api/modules/atomic.html" target="_blank">![](http://atomicgameengine.com/images/community/Documentation_JSTS.png)</a>
<a href="http://docs.atomicgameengine.com/csharp/AtomicEngine/" target="_blank">![](http://atomicgameengine.com/images/community/Documentation_CSharp.png)</a>
<a href="http://docs.atomicgameengine.com/cpp" target="_blank">![](http://atomicgameengine.com/images/community/Documentation_CPlusPlus.png)</a>

---
