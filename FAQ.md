![](https://s16.postimg.org/po3hqgsid/frequentlyaskedquesitons.png)

## FAQ

### I'm stuck. Where can I get help?
If you've got any problems or questions, please visit the **[Atomic Support Forum](https://discourse.atomicgameengine.com/)**. There, the whole community can see your question and will hopefully be able to help you out. We also have an **[Atomic Chat](https://gitter.im/AtomicGameEngine/AtomicGameEngine)** channel with many helpful folks (and a surprising number of 🐫 emoji!)
