Back to [Getting Started](Getting-Started)

![](images/repo/Getting_Started/basic-materials/header.png)

### Basic materials

In 3D projects, materials can be assigned to your 3D models. Materials contain textures and a few modifiable values. In this overview, we'll take a short look on how to use them.

**Setup**<br/>
First, I have created a project and imported a 3D model into the default scene (`Scene.scene`): 
![](images/repo/Getting_Started/basic-materials/setup.png)
Now, I want to add a material to this chair to make it look a tad more fancy.<br/>

<img src="images/repo/Getting_Started/basic-materials/createNewMaterial.png" align="left" width="300">
**1) Create a new material**<br/>
Start out by creating a new material that we are going to be modifying and using. In the Project Section (see [The Atomic Editor](the-atomic-editor) if you're in doubt), right-click and choose `Create Material`. In this case, I create a new material inside the `Materials` folder.
<br/><br/><br/><br/><br/>
<img src="images/repo/Getting_Started/basic-materials/modifyMaterial.png" align="left" width="300" vspace="19">
**2) Modify the material**<br/>
When the material has been created, you'll be able to see its values in the inspector. In this overview, we won't cover all these features - the focus will be on applying a texture to this material, and thereafter putting it onto the chair model.<br/>
Locate the `Texture` section inside the inspector, click on the `diffuse` dropdown and navigate to a texture that you have imported into your project. Now, click on `Save` on the bottom of the inspector.<br/>

<br/>
**3) Apply the material to the object**<br/>
Now, you can drag the material from the Project Section onto your object. The 3D viewport will automatically be updated, and you'll be able to see the material.<br/>
![](images/repo/Getting_Started/basic-materials/applyMaterial.gif)



