### Introduction

The Atomic Editor application is a UI and script extension of the native ToolCore library.  The editor is primarily extended with TypeScript which is located under Script/AtomicEditor in the repository.

This is a recipe for setting up a basic TypeScript environment for working on the Atomic Editor (not projects).  The method described uses VSCode. It is also possible to use Atom with the TypeScript plugin. 

### Download Visual Studio Code

[![Visual Studio Code](https://raw.githubusercontent.com/wiki/AtomicGameEngine/AtomicGameEngine/images/repo/cookbook/vscode.png)](https://code.visualstudio.com/)

### Setting up for Atomic Editor development

This recipe will take a varying amount of time based on what tools are installed.  At least 15 minutes will be required to complete these steps:
   
1. Build the Atomic Editor from source.  This isn't as scary as it may sound using these [simple one line instructions](Building-Atomic-from-Source).

  On Windows: 

  [[images/repo/cookbook/build_editor_win.png]]

  On macOS/Linux:

  [[images/repo/cookbook/build_editor_mac.png]]

2. In your local source tree, open *AtomicGameEngine*/Script/tsconfig.json and find the line

  ```json
"outDir": "../Artifacts/Build/Resources/EditorData/AtomicEditor/EditorScripts",
```

3. Depending on your platform change this line to:

  **Windows/Linux**

  ```json
"outDir": "../Artifacts/AtomicEditor/Resources/EditorData/AtomicEditor/EditorScripts",
```

  **macOS**

  ```json
"outDir": "../Artifacts/AtomicEditor/AtomicEditor.app/Contents/Resources/EditorData/AtomicEditor/EditorScripts",
```

  (This will change the compiled TypeScript to be output in the built Atomic Editor instead of the normal staging location)

4. Open the *AtomicGameEngine*/Script folder in VSCode and configure a Task Runner - you can do this by opening the Command Palette (`View > Command Palette` or `CMD`/`CTRL + SHIFT + P`) and finding `Tasks: Configure Task Runner`.
Select `TypeScript: tsconfig.json` in the task runners available.  

5. Open ```AtomicEditor/main.ts``` and modify the main method to output some text by adding ```console.log("Hello Atomic Editor!");``` then build by pressing `CMD`/`CTRL + SHIFT + B` (If you're on macOS/Linux and receive an error, you may have to install TypeScript using `npm install -g typescript`)

  [[images/repo/cookbook/atomiceditor_ts_setup.png]]

  The Atomic Editor binary built in step 1 is in the Artifacts/AtomicEditor folder, run it and open the developer console using the ```Developer > Show Console``` menu.  Near the top of the console log you should see ```"Hello Atomic Editor!"```

  [[images/repo/cookbook/atomiceditor_run.png]]

  [[images/repo/cookbook/atomiceditor_script.png]]

6.  Congratulations! You can now edit and add new Atomic Editor TypeScript with VSCode!  

