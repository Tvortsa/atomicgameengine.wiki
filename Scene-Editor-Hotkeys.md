Back to [Getting Started](Getting-Started)

Hotkey             | Description                       |
------------------| --------------------------------- |
W, A, S, D + RMB | Move the camera forward, back, left, right (*scroll mouse wheel to adjust speed*) |
Q, E + RMB | Move the camera up, down (*scroll mouse wheel to adjust speed*) |
Right mouse drag | Rotate Camera |
F | Frame Selection |
Alt + Left mouse drag | Orbit Selection |
Alt + Mouse wheel | Zoom camera at mouse position |
W | Translate Gizmo |
E | Rotate Gizmo |
R | Scale Gizmo |
X | Toggle Local/World axis mode |
P | Perspective View |
0 | Orthographic View Toggle (1-6 switches views): 1 - Top, 2 - Bottom, 3 - Left, 4 - Right, 5 - Front, 6 - Back |
G | Grid Toggle |
Shift + Left mouse drag | Duplicate Object |