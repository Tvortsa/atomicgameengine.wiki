### Android Deployment - Building From the Editor

Once the [setup steps](Android-Deployment-Setup) are completed, the project is ready to be deployed from the editor.

---

#### Step 1: Name the Application

_Shift + Ctrl (Cmd ) + B_ – Opens the Build Settings dialog. Click the Android icon and select _Set Current Platform_.

Assign App Name, App Package (_a unique name using app store naming guidelines_), Company Name, and Product Name.

[[images/android/Atomic_Android_AppName.png]]

---

#### Step 2: Add the Paths to the Build Settings

* Assign the Android SDK Path. Once this is selected the API Level should update automatically, if not, click _Refresh_.
* Assign the Ant Path (_this is the folder that contains ant.bat_)
* Assign the JDK Root (_windows only_)

[[images/android/Atomic_Android_BuildSettings.png]]

Click _Ok_ to save the changes.

---

#### Step 3: Build the Project

_Ctrl (Cmd) + B_ – Opens the Build window

Choose a folder to save the deployment. The Android-Build folder will be generated here, this folder  contains the application's libraries and serialized assets.

[[images/android/Atomic_Build.PNG]]  

Click _Build_  

The application will launch on device once the build is complete.  
