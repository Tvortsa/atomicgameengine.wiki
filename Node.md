Back to [Getting Started](Getting-Started)

![](images/repo/Getting_Started/node/header.png)

# Nodes

A node is an instance in your project. For example, 3D & 2D instances, lights and cameras are all nodes.
Detailed information about nodes can be found at the [Urho3D wiki](https://github.com/urho3d/Urho3D/wiki/First-Project#basic-functionality-the-node-model).

![](images/repo/Getting_Started/node/introduction.png)

> Just as all other 3D models and/or 2D sprites in your scene, these crates and the pallet are nodes. In the Inspector, the first component seen is the “Node” component which contains the name, position/rotation/scale values and prefab options for the selected node.

---

### Creating new nodes

<img src="images/repo/Getting_Started/node/createNewNode.png" align="left"/>**In the Atomic Editor**<br/>
In the Hierarchy, click on the “Create” button and click “Node”. An empty node will appear in your scene. To add a 3D mesh to this node, add the StaticModel component to this node as well.
<br/><br/>

**With scripting**<br/>
JavaScript:<br/>
```
var newNode = Atomic.game.scene.createChild("nodeName");
```

C#:<br/>
```
var newNode = Scene.CreateChild("nodeName");
```
