[Back to Cookbook](Atomic-Cookbook)

## Updating User Preferences

You can change the settings in the preferences file located at:  

**Windows**: C:\Users\<UserName>\AppData\Roaming\AtomicEditor\Preferences\prefs.json  
**OSX**: ~/Library/Application Support/AtomicEditor/Preferences/prefs.json  
**Linux**: ~/.local/share/AtomicEditor/Preferences/prefs.json

Default settings:

```
  "codeEditor": {
    "theme": "vs-dark",
    "fontSize": 12,
    "fontFamily": "",
    "showInvisibles": false,
    "useSoftTabs": true,
    "tabSize": 4
  }
```

* **theme**: can be "vs-dark" or "vs"  
* **fontSize**: The point size of the editor font  
* **fontFamily**: If populated will use the font style provided instead of the built-in default  
* **showInvisibles**: true or false - show spaces/tabs in the editor  
* **useSoftTabs**: true or false - use spaces in place of tabs  
* **tabSize**: The number of spaces in each tab  

## Changing Editor Skin

You can also switch the editor from a dark theme to a light theme by modifying the following key in the prefs file:

```
"uiData": {
    ...
    "defaultSkinPath": "AtomicEditor/resources/default_skin_light"
}
```