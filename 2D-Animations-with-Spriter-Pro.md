Back to [Getting Started](Getting-Started)

![](images/repo/Getting_Started/2d-animations-with-spriter-pro/header.png)

### 2D Animations with Spriter Pro

---

Atomic integrates with [Spriter Pro](https://brashmonkey.com/) and can use its project files for you to take advantage of in your projects. <br/>
In this overview, we'll be looking at how to import projects from Spriter Pro and using them in Atomic. The result of these instructions will be a beating image (in this case, the MonoDevelop logo), and it'll look like this: <br/>

> ![](images/repo/Getting_Started/2d-animations-with-spriter-pro/result.gif) <br/> *The animation at runtime*

## 1) Creating a project in Spriter Pro <br/>
First, we'll create a new project inside of Spriter. <br/>
![](images/repo/Getting_Started/2d-animations-with-spriter-pro/createSpriterProject.png) <br/>
<br/>
Spriter will ask you to choose the root folder for your project. To make life easier for us, we'll choose the `Sprites` folder inside of our Atomic project folder. <br/>
<img src="images/repo/Getting_Started/2d-animations-with-spriter-pro/chooseRootFolder.png" width="555"> <br/>

Spriter will now prepare your project. It should look something like this: <br/>
![](images/repo/Getting_Started/2d-animations-with-spriter-pro/spriterWindow.png) <br/>
Spriter will display all the images files in this location (the `Sprites` folder) to the right.

## 2) Use Spriter's tools to create animation(s) <br/>
This overview won't go into detail on how to create animations with Spriter, so let's assume that you have created one or more animations at this point, using Spriter's tools. </br>
One thing worth mentioning, however, is that you should remember to name your animation(s): <br/>
![](images/repo/Getting_Started/2d-animations-with-spriter-pro/spriterAnimationName.png) <br/>
In this case, the animation name is `Default`.

## 3) Importing the animation into Atomic <br/>
<img src="images/repo/Getting_Started/2d-animations-with-spriter-pro/createEmptyNode.png" align="left">
**Create a new empty node**<br/>
Select your scene inside of Atomic, and in the Hierarchy section, use `Create` > `Node` to make a new [node](node). <br/> <br/>
<img src="images/repo/Getting_Started/2d-animations-with-spriter-pro/componentOverview.png" align="left" width="300">
**Create an `AnimatedSprite2D` component** <br/>
In the inspector, click on `Create Component` and navigate to `2D` > `AnimatedSprite2D`. <br/><br/>
**Modify the component to your needs** <br/>
There are three main fields you need to take care of: <br/><br/>
*Entity*: Whatever you have set the name of this entity inside of Spriter. <br/><br/>
*Animation Set*: When clicking on the three dots, Atomic will present you with all files inside your project with the Spriter Pro project extension *(.scml)*. Choose the desired project file. <br/><br/>
*Animation*: Whatever animation you want to be played. In this case, the animation I want to play is named `Default`. <br/><br/>
<div><img src="images/repo/Getting_Started/2d-animations-with-spriter-pro/modifyValues.png" vspace=30"></div>
## Conclusion
<img src="images/repo/Getting_Started/2d-animations-with-spriter-pro/conclusion.png" align="left" width="177">
You should be able to see a thumbnail from the animation inside Atomic's 3D viewport. When running the application, the [result](https://github.com/AtomicGameEngine/AtomicGameEngine/wiki/2D-Animations-with-Spriter-Pro#2d-animations-with-spriter-pro) should be displayed.